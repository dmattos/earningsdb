# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 00:53:10 2020

@author: lins_
"""

import sys
sys.path.append('C:\\Users\\lins_\\webdrivers')
#If necessary, replace arguments with: 
#'/usr/local/lib/python2.7/dist-packages'
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import pandas as pd
import numpy as np
import time
import re

options = Options()
#options.add_argument('--headless')
options.add_argument('--disable-gpu')
driver = webdriver.Chrome('C:/Users/lins_/webdrivers/chromedriver.exe',
                              options=options)
actions = ActionChains(driver)

url = 'https://www.rad.cvm.gov.br/ENET/frmConsultaExternaCVM.aspx'
driver.get(url)

while 1:
    try:
        driver.find_element_by_id('rdPeriodo').click()
        break
    except NoSuchElementException:
        time.sleep(0.1)
    except ElementClickInterceptedException:
        time.sleep(0.1)
        
while 1:
    try:
        element = driver.find_element_by_id('cboEmpresa').send_keys('023574')
        time.sleep(0.1)
        actions.send_keys(Keys.ARROW_DOWN).perform()    
        time.sleep(0.1)
        actions.send_keys(Keys.ENTER).perform()
        break
    except NoSuchElementException:
        time.sleep(0.1)

while 1:
    try:
        driver.find_element_by_id('txtDataIni').send_keys('01/01/2011')
        actions.send_keys(Keys.TAB).perform()
        driver.find_element_by_id('txtDataFim').send_keys(time.strftime('%d/%m/%Y'))
        actions.send_keys(Keys.TAB).perform()
        break
    except NoSuchElementException:
        time.sleep(0.1)


########################################################################
################################ DFPs ##################################
########################################################################
        
while 1:
    try:
        driver.find_element_by_class_name('chosen-single').click()
        listItems = driver.find_elements_by_xpath('//div[@class="chosen-drop"]/ul/li')

        for item in listItems: # <li> tags list
            if item.text == "DFP": # select index 
                item.click() # if index=0 click
                break
        break
    except NoSuchElementException:
       time.sleep(0.1)

while 1:
    try:
        driver.find_element_by_id('btnConsulta').click()
        break
    except NoSuchElementException:
        time.sleep(0.1)

time.sleep(0.5)

datas_ref = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(6)")]
datas_dlv = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(7)")]
status = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(8)")]
links = [re.findall(r"'(.*?)'", x.get_attribute("onclick"), re.DOTALL)[0] for x in driver.find_elements_by_id('VisualizarDocumento')]

#Clean datas ref
while 1:
    try:
        datas_ref.remove('')
    except ValueError:
        break

#Clean datas dlv
while 1:
    try:
        datas_dlv.remove('')
    except ValueError:
        break

#Clean datas dlv
while 1:
    try:
        status.remove('')
    except ValueError:
        break

#Clean links
while 1:
    try:
        links.remove('')
    except ValueError:
        break

dfDFP = pd.DataFrame(data=np.matrix([datas_ref,datas_dlv,status,links,["DFP"]*\
                  len(links)]).transpose(), columns=\
                  ["Periodo","Entrega", "Status", "Link","Tipo"])

dfDFP = dfDFP.drop(dfDFP[dfDFP.Status == 'Inativo'].index)
driver.close()

########################################################################
################################ ITRs ##################################
########################################################################


driver = webdriver.Chrome('C:/Users/lins_/webdrivers/chromedriver.exe',
                              options=options)
actions = ActionChains(driver)

url = 'https://www.rad.cvm.gov.br/ENET/frmConsultaExternaCVM.aspx'
driver.get(url)

while 1:
    try:
        driver.find_element_by_id('rdPeriodo').click()
        break
    except NoSuchElementException:
        time.sleep(0.1)
    except ElementClickInterceptedException:
        time.sleep(0.1)
        
while 1:
    try:
        element = driver.find_element_by_id('cboEmpresa').send_keys('023574')
        time.sleep(0.1)
        actions.send_keys(Keys.ARROW_DOWN).perform()    
        time.sleep(0.1)
        actions.send_keys(Keys.ENTER).perform()
        break
    except NoSuchElementException:
        time.sleep(0.1)

while 1:
    try:
        driver.find_element_by_id('txtDataIni').send_keys('01/01/2011')
        actions.send_keys(Keys.TAB).perform()
        driver.find_element_by_id('txtDataFim').send_keys(time.strftime('%d/%m/%Y'))
        actions.send_keys(Keys.TAB).perform()
        break
    except NoSuchElementException:
        time.sleep(0.1)

while 1:
    try:
        driver.find_element_by_class_name('chosen-single').click()
        listItems = driver.find_elements_by_xpath('//div[@class="chosen-drop"]/ul/li')

        for item in listItems: # <li> tags list
            if item.text == "ITR": # select index 
                item.click() # if index=0 click
                break
        break
    except NoSuchElementException:
       time.sleep(0.1)

while 1:
    try:
        driver.find_element_by_id('btnConsulta').click()
        break
    except NoSuchElementException:
        time.sleep(0.1)

time.sleep(0.5)

datas_ref = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(6)")]
datas_dlv = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(7)")]
status = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(8)")]
links = [re.findall(r"'(.*?)'", x.get_attribute("onclick"), re.DOTALL)[0] for x in driver.find_elements_by_id('VisualizarDocumento')]

#Clean datas ref
while 1:
    try:
        datas_ref.remove('')
    except ValueError:
        break

#Clean datas dlv
while 1:
    try:
        datas_dlv.remove('')
    except ValueError:
        break

#Clean datas dlv
while 1:
    try:
        status.remove('')
    except ValueError:
        break

#Clean links
while 1:
    try:
        links.remove('')
    except ValueError:
        break

driver.close()

dfITR = pd.DataFrame(data=np.matrix([datas_ref,datas_dlv,status,links,["ITR"]*\
                  len(links)]).transpose(), columns=\
                  ["Periodo","Entrega", "Status", "Link","Tipo"])
    
dfITR = dfITR.drop(dfITR[dfITR.Status == 'Inativo'].index)

df = pd.concat([dfDFP,dfITR])

df.Periodo = pd.to_datetime(df.Periodo,dayfirst=True)
df.Entrega = pd.to_datetime(df.Entrega,dayfirst=True)
df = df.sort_values(by=['Periodo'],ascending=False)

