
# A very simple Flask Hello World app for you to get started with...

from flask import Flask, redirect, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy
import DB_Management
from DB_Management import *

app = Flask(__name__)
app.config["DEBUG"] = True

engine = create_engine('mysql+mysqlconnector://lins502:vandamme@lins502.mysql.pythonanywhere-services.com/lins502$earningsdb', pool_recycle=280)
Petrobras = series_load('Petrobras','9512',engine)

commands = list()
comments = list()

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        return render_template("main_page.html", comments=comments)

    commands.append(str(request.form["contents"]))
    if commands.pop() == 'PETR4 Price':
        comments.append(str(Petrobras.preferred_prices.iloc[-1,3]))
    else:
        comments.append('Command not recognized')
    return redirect(url_for('index'))