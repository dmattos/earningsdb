# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 13:28:50 2017

@author: Daniel Mattos
"""

import sys
sys.path.append('C:\\Users\\lins_\\webdrivers')
#If necessary, replace arguments with: 
#'/usr/local/lib/python2.7/dist-packages'
from selenium.common.exceptions import NoSuchElementException
from statsmodels.tsa.seasonal import seasonal_decompose
from selenium.webdriver.chrome.options import Options
from pandas_datareader import data as pdr
from datetime import datetime, timedelta
from selenium import webdriver
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import fix_yahoo_finance as yf
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import pickle
import time
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import re

### Dictionary for Converting URLs ###
urlDic = {
    'ç': '%C3%A7',
    'ã': '%C3%A3',
    ' ': '%20',
    '&amp;': '&'
}

### Dictionary for Converting Special Characters in HTML Statements  ###
strDic = {
    '\xa0': ''
}

### Dictionary for Converting Dates to Corresponding Quarters ###
dateDic = {
    '01/01': 0,
    '31/03': 1,
    '01/04': 1,
    '30/06': 2,
    '01/07': 2,
    '30/09': 3,
    '01/10': 3,
    '31/12': 4
}

### Closest Date to a Given Date ###
def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))

### Convert BR Date With Slash to Standard Format With Dash ###
def convert_date(BR_Date):
    year = BR_Date[6:10]
    month = BR_Date[3:5]
    day = BR_Date[0:2]
    return year+'-'+month+'-'+day

### Get Number of Quarters Between Dates ###
def span(date):
    beg = dateDic[date[0:5]]
    end = dateDic[date[11:16]]
    if end > beg:
        return end - beg
    else:
        return (4-beg+end)

### Handles Possible Date Inconsistency in Statements ###
def check_DF_date_consistency(statement):
    if statement.columns.values[1]==statement.columns.values[2]:
        if statement.iloc[0,1]==statement.iloc[0,2]:
            return 1
        elif statement.iloc[0,1]==0 and statement.iloc[0,2]!=0:
            return 2
        elif statement.iloc[0,1]!=0 and statement.iloc[0,2]==0:
            return 1
        else:
            return 0
    else:
        return 1

### Check if Each Statement Corresponds to the Right Number as per CVM Convention ###
def check_DF_items_consistency(series):
    error = False
    for i in series.DF:
        if i.IS.index.values[0][0]=='3' and\
            i.CF.index.values[0][0]=='6' and\
            i.BS.index.values[0][0]=='1' and\
            i.BS.index.values[len(i.BS.index.values)-1][0]=='2':
            pass
        else:
            error = True
    return error

### Replace Text According to Dictionary ###
def multipleReplace(text, wordDict):

    for key in wordDict:
        try:
            text = text.replace(key, wordDict[key])
        except AttributeError:
            text = text
        except IndexError:
            text = text
    return text

### Remove Thousands Separator and Replace Decimal Separator ###
def valor(my_str):
    try:
        val = float(my_str.replace('.','').replace(',','.'))
    except ValueError:
        val = ''
    return val

### Ordered Insert DFP Date Into List of ITRs ###
def insert_DF(series,element):
    for i in range(0,len(series)):
        if datetime.strptime(element.filing_date, "%d/%m/%Y") > datetime.strptime(series[i].filing_date, "%d/%m/%Y"):
            series[i:i] = [element]
            break
    return series

### Latest Web Scraping Tool for CVM Financials Data - NOT FUNCTIONAL! ###
def get_data_combined_fast(cod,driver):
    dfCode = [['4','Demonstra%c3%a7%c3%a3o+do+Resultado','Demonstra%C3%A7%C3%A3o%20do%20Resultado'],
              ['99','Demonstra%c3%a7%c3%a3o+do+Fluxo+de+Caixa','Demonstra%C3%A7%C3%A3o%20do%20Fluxo%20de%20Caixa'],
              ['2','Balan%c3%a7o+Patrimonial+Ativo','Balan%C3%A7o%20Patrimonial%20Ativo'],
              ['3','Balan%c3%a7o+Patrimonial+Passivo','Balan%C3%A7o%20Patrimonial%20Passivo&Empresa']]
    while 1:
        try:
            url_base = 'https://www.rad.cvm.gov.br/ENET/'
            url = url_base+cod
            driver.get(url)
            break
        except IndexError:
            time.sleep(2)
    address_list = list()
    while 1:
        try:
            soup=BeautifulSoup(driver.page_source, "html.parser")
            scripts = soup.find_all('script')
            c = scripts[len(scripts)-1].get_text()
            url_compl=c[(c.index('\'')+1):(c.index('\'')+1+c[(c.index('\'')+1):].index('\''))]
            urlFrame=multipleReplace(url_base+url_compl,urlDic)
            break
        except ValueError:
            time.sleep(0.1)
    #Decompose address
    p_1 = urlFrame[0:(urlFrame.index('&Demonstracao=')+len('&Demonstracao='))]
    p_2 = urlFrame[urlFrame.index('&Periodo='):(urlFrame.index('&Quadro=')+len('&Quadro='))]
    p_3 = urlFrame[urlFrame.index('&NomeTipoDocumento='):(urlFrame.index('&CodigoTipoInstituicao')+len('&CodigoTipoInstituicao=1'))]
    for i in dfCode:
        address_list.append(p_1+i[0]+p_2+i[1]+p_3)
    info = list()
    for address in address_list:
        while 1:
            try:
                driver.get(address)
                break
            except IndexError:
                time.sleep(2)
        soup = BeautifulSoup(driver.page_source, "html.parser")
        table_code = soup.findAll("tr")
        columns = list()
        for i in table_code[0]("td"):
            columns.append(i.text.replace("à",";").replace("\xa0",""))
        rows = list()
        rows_index = list()
        for i in table_code[1:]:
            row = list()
            rows_index.append(multipleReplace(i("td")[0].string,strDic))
            row.append(multipleReplace(i("td")[1].string,strDic))
            for j in range(2,len(i("td"))):
                row.append(valor(multipleReplace(i("td")[j].string,strDic)))
            rows.append(row)
        info.append(pd.DataFrame(rows,index=rows_index,columns=columns[1:]))

    #Decompose address of capital structure
    p_1 = urlFrame[0:(urlFrame.index('enetconsulta/')+len('enetconsulta/'))]
    p_2 = urlFrame[urlFrame.index('&NomeTipoDocumento='):(urlFrame.index('&CodigoTipoInstituicao')+len('&CodigoTipoInstituicao=1'))]
    address = p_1+'frmDadosComposicaoCapitalITR.aspx?Grupo=Dados+da+Empresa&Quadro=Composi%c3%a7%c3%a3o+do+Capital'+p_2
    driver.get(address)
    soup = BeautifulSoup(driver.page_source, "html.parser")
    elements = soup.find_all('td')
    if('(Mil)' in elements[0].text):
        total_common = valor(elements[4].getText())*1000
        treasury_common = valor(elements[11].getText())*1000
        total_preferred = valor(elements[6].getText())*1000
        treasury_preferred = valor(elements[13].getText())*1000
    else:
        total_common = valor(elements[4].getText())
        treasury_common = valor(elements[11].getText())
        total_preferred = valor(elements[6].getText())
        treasury_preferred = valor(elements[13].getText())

    info.append(total_common-treasury_common)
    info.append(total_preferred-treasury_preferred)
    return info

### Get Single DF ###
def get_data(cod,driver,info_type,info_spec):
    while 1:
        try:    
            url_base = 'https://www.rad.cvm.gov.br/ENET/'
            url = url_base+cod
            driver.get(url)
            elements = Select(driver.find_element_by_name('cmbGrupo')) #Menu da esquerda
            elements.select_by_visible_text(info_type)
            elements = Select(driver.find_element_by_name('cmbQuadro')) #Menu da direita
            elements.select_by_visible_text(info_spec)
            soup=BeautifulSoup(driver.page_source, "html.parser")
            scripts = soup.find_all('script')
            c = scripts[len(scripts)-1].string
            url_compl=c[(c.index('\'')+1):(c.index('\'')+1+c[(c.index('\'')+1):].index('\''))]
            urlFrame=multipleReplace(url_base+url_compl,urlDic)
            driver.get(urlFrame)
            soup = BeautifulSoup(driver.page_source, "html.parser")
            table_code = soup.findAll("tr")
            columns = list()
            for i in table_code[0]("td"): 
                columns.append(i.text.replace("à",";").replace("\xa0",""))
            rows = list()
            rows_index = list()
            for i in table_code[1:]:
                row = list()
                rows_index.append(multipleReplace(i("td")[0].string,strDic))
                row.append(multipleReplace(i("td")[1].string,strDic))
                for j in range(2,len(i("td"))):
                    row.append(valor(multipleReplace(i("td")[j].string,strDic)))
                rows.append(row)
            return pd.DataFrame(rows,index=rows_index,columns=columns[1:])
        except IndexError:
            time.sleep(2)

### Get Number of Shares ###
def get_nb_shares(cod,driver,info_type,info_spec):
    while 1:
        try:    
            url_base = 'https://www.rad.cvm.gov.br/ENET/'
            url = url_base+cod
            driver.get(url)
            elements = Select(driver.find_element_by_name('cmbGrupo')) #Menu da esquerda
            elements.select_by_visible_text(info_type)
            elements = Select(driver.find_element_by_name('cmbQuadro')) #Menu da direita
            elements.select_by_visible_text(info_spec)
            soup=BeautifulSoup(driver.page_source, "html.parser")
            scripts = soup.find_all('script')
            c = scripts[len(scripts)-1].string
            url_compl=c[(c.index('\'')+1):(c.index('\'')+1+c[(c.index('\'')+1):].index('\''))]
            urlFrame=multipleReplace(url_base+url_compl,urlDic)
            driver.get(urlFrame)
            soup = BeautifulSoup(driver.page_source, "html.parser")
            elements = soup.find_all('td')
            if('(Mil)' in elements[0].text):
                total_common = valor(elements[4].getText())*1000
                treasury_common = valor(elements[11].getText())*1000
                total_preferred = valor(elements[6].getText())*1000
                treasury_preferred = valor(elements[13].getText())*1000
            else:
                total_common = valor(elements[4].getText())
                treasury_common = valor(elements[11].getText())
                total_preferred = valor(elements[6].getText())
                treasury_preferred = valor(elements[13].getText())
            info = list()
            info.append(total_common-treasury_common)
            info.append(total_preferred-treasury_preferred)
            return info
        except IndexError:
            time.sleep(2)

### Fetch All Available Financials Available in the Latest Format ###
def get_series(Curr_DFs,ccvm,cnpj,last_n):
    
    ########################################################################
    ############################## Get DFs #################################
    ########################################################################
    
    options = Options()
    #options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    driver = webdriver.Chrome('C:/Users/lins_/webdrivers/chromedriver.exe',
                                  options=options)
    actions = ActionChains(driver)
    
    url = 'https://www.rad.cvm.gov.br/ENET/frmConsultaExternaCVM.aspx'
    driver.get(url)
    
    while 1:
        try:
            driver.find_element_by_id('rdPeriodo').click()
            break
        except NoSuchElementException:
            time.sleep(0.1)
        except ElementClickInterceptedException:
            time.sleep(0.1)
            
    while 1:
        try:
            element = driver.find_element_by_id('cboEmpresa').send_keys(ccvm)
            time.sleep(0.1)
            actions.send_keys(Keys.ARROW_DOWN).perform()    
            time.sleep(0.1)
            actions.send_keys(Keys.ENTER).perform()
            break
        except NoSuchElementException:
            time.sleep(0.1)
    
    while 1:
        try:
            driver.find_element_by_id('txtDataIni').send_keys('01/01/2011')
            actions.send_keys(Keys.TAB).perform()
            driver.find_element_by_id('txtDataFim').send_keys(time.strftime('%d/%m/%Y'))
            actions.send_keys(Keys.TAB).perform()
            break
        except NoSuchElementException:
            time.sleep(0.1)
    
    
    ########################################################################
    ################################ DFPs ##################################
    ########################################################################
            
    while 1:
        try:
            driver.find_element_by_class_name('chosen-single').click()
            listItems = driver.find_elements_by_xpath('//div[@class="chosen-drop"]/ul/li')
    
            for item in listItems: # <li> tags list
                if item.text == "DFP": # select index 
                    item.click() # if index=0 click
                    break
            break
        except NoSuchElementException:
           time.sleep(0.1)
    
    while 1:
        try:
            driver.find_element_by_id('btnConsulta').click()
            break
        except NoSuchElementException:
            time.sleep(0.1)
    
    time.sleep(0.5)
    
    datas_ref = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(6)")]
    datas_dlv = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(7)")]
    status = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(8)")]
    links = [re.findall(r"'(.*?)'", x.get_attribute("onclick"), re.DOTALL)[0] for x in driver.find_elements_by_id('VisualizarDocumento')]
    
    #Clean datas ref
    while 1:
        try:
            datas_ref.remove('')
        except ValueError:
            break
    
    #Clean datas dlv
    while 1:
        try:
            datas_dlv.remove('')
        except ValueError:
            break
    
    #Clean datas dlv
    while 1:
        try:
            status.remove('')
        except ValueError:
            break
    
    #Clean links
    while 1:
        try:
            links.remove('')
        except ValueError:
            break
    
    dfDFP = pd.DataFrame(data=np.matrix([datas_ref,datas_dlv,status,links,["DFP"]*\
                      len(links)]).transpose(), columns=\
                      ["Periodo","Entrega", "Status", "Link","Tipo"])
    
    dfDFP = dfDFP.drop(dfDFP[dfDFP.Status == 'Inativo'].index)
    driver.close()
    
    ########################################################################
    ################################ ITRs ##################################
    ########################################################################
    
    
    driver = webdriver.Chrome('C:/Users/lins_/webdrivers/chromedriver.exe',
                                  options=options)
    actions = ActionChains(driver)
    
    url = 'https://www.rad.cvm.gov.br/ENET/frmConsultaExternaCVM.aspx'
    driver.get(url)
    
    while 1:
        try:
            driver.find_element_by_id('rdPeriodo').click()
            break
        except NoSuchElementException:
            time.sleep(0.1)
        except ElementClickInterceptedException:
            time.sleep(0.1)
            
    while 1:
        try:
            element = driver.find_element_by_id('cboEmpresa').send_keys(ccvm)
            time.sleep(0.1)
            actions.send_keys(Keys.ARROW_DOWN).perform()    
            time.sleep(0.1)
            actions.send_keys(Keys.ENTER).perform()
            break
        except NoSuchElementException:
            time.sleep(0.1)
    
    while 1:
        try:
            driver.find_element_by_id('txtDataIni').send_keys('01/01/2011')
            actions.send_keys(Keys.TAB).perform()
            driver.find_element_by_id('txtDataFim').send_keys(time.strftime('%d/%m/%Y'))
            actions.send_keys(Keys.TAB).perform()
            break
        except NoSuchElementException:
            time.sleep(0.1)
    
    while 1:
        try:
            driver.find_element_by_class_name('chosen-single').click()
            listItems = driver.find_elements_by_xpath('//div[@class="chosen-drop"]/ul/li')
    
            for item in listItems: # <li> tags list
                if item.text == "ITR": # select index 
                    item.click() # if index=0 click
                    break
            break
        except NoSuchElementException:
           time.sleep(0.1)
    
    while 1:
        try:
            driver.find_element_by_id('btnConsulta').click()
            break
        except NoSuchElementException:
            time.sleep(0.1)
    
    time.sleep(0.5)
    
    datas_ref = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(6)")]
    datas_dlv = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(7)")]
    status = [x.text for x in driver.find_elements_by_css_selector("td:nth-child(8)")]
    links = [re.findall(r"'(.*?)'", x.get_attribute("onclick"), re.DOTALL)[0] for x in driver.find_elements_by_id('VisualizarDocumento')]
    
    #Clean datas ref
    while 1:
        try:
            remove_data_ref = [i for i, e in enumerate(datas_ref) if len(e) < 10]
            break
        except ValueError:
            break
    
    #Clean datas dlv
    while 1:
        try:
            remove_data_dlv = [i for i, e in enumerate(datas_dlv) if len(e) < 10]
            break
        except ValueError:
            break
    
    #Clean datas dlv
    while 1:
        try:
            remove_status = [i for i, e in enumerate(status) if e not in ['Ativo','Inativo']]
            break
        except ValueError:
            break
    
    #Clean links
    while 1:
        try:
            remove_links=[i for i, e in enumerate(status) if e.find('frmGerenciaPaginaFRE.aspx?NumeroSequencialDocumento=')==0]
            break
        except ValueError:
            break
    
    driver.close()
    
    remove = remove_data_ref + list(set(remove_data_dlv) - set(remove_data_ref))
    remove = remove + list(set(remove_status) - set(remove))
    remove = remove + list(set(remove_links) - set(remove))
    
    datas_ref = [e for i, e in enumerate(datas_ref) if i not in remove]
    datas_dlv = [e for i, e in enumerate(datas_dlv) if i not in remove]
    status = [e for i, e in enumerate(status) if i not in remove]
    links = [e for i, e in enumerate(links) if i not in remove]
    
    dfITR = pd.DataFrame(data=np.matrix([datas_ref,datas_dlv,status,links,["ITR"]*\
                      len(links)]).transpose(), columns=\
                      ["Periodo","Entrega", "Status", "Link","Tipo"])
        
    dfITR = dfITR.drop(dfITR[dfITR.Status == 'Inativo'].index)
    
    df = pd.concat([dfDFP,dfITR])
    
    df.Periodo = pd.to_datetime(df.Periodo,dayfirst=True)
    df.Entrega = pd.to_datetime(df.Entrega,dayfirst=True)
    df = df.sort_values(by=['Periodo'],ascending=False)
    
    ########################################################################
    ################################ End ###################################
    ########################################################################
    
    driver = webdriver.Chrome('C:/Users/lins_/webdrivers/chromedriver.exe',
                                  options=options)
    series = list()
    for i in range(0,len(df)):
        x=DFs(df.iloc[i,3],df.iloc[i,0].strftime('%d/%m/%Y'),df.iloc[i,1].strftime('%d/%m/%Y'))
        series.append(x)   

    pre_existing_DFs = list()
    for i in Curr_DFs:
        pre_existing_DFs.append(i.filing_date)

    earliest = last_n
    if len(pre_existing_DFs)!=0:
        for n in range(0,len(series)):
            if pre_existing_DFs[len(pre_existing_DFs)-1]==series[n].filing_date:
                earliest=n+1
                break

    for i in series[0:min(max(last_n,earliest),len(series))]:
        if not(i.filing_date in pre_existing_DFs):
            IS = get_data(i.cod,driver,'DFs Consolidadas','Demonstração do Resultado')
            CF = get_data(i.cod,driver,'DFs Consolidadas','Demonstração do Fluxo de Caixa')
            BS = get_data(i.cod,driver,'DFs Consolidadas','Balanço Patrimonial Ativo')
            BS = BS.append(get_data(i.cod,driver,'DFs Consolidadas','Balanço Patrimonial Passivo'))
            Shares = get_nb_shares(i.cod,driver,'Dados da Empresa','Composição do Capital')
            
            i.IS = IS
            i.CF = CF
            i.BS = BS
            
            i.common_shares = Shares[0]
            i.preferred_shares = Shares[1]
        else:
            for j in Curr_DFs:
                if j.filing_date == i.filing_date:
                    i.IS = j.IS
                    i.CF = j.CF
                    i.BS = j.BS
                    i.common_shares=j.common_shares
                    i.preferred_shares=j.preferred_shares
                    break

    driver.quit()
    while series[len(series)-1].IS is None:
        series.pop()
    return series

### Get Quarterly Result for Specified Accounts and Date in the IS ###
def get_Q_Account_IS(series,date,index=[],dic=[]):
    try:
        if index == []:
            for i,items in enumerate(series.DF):
                period = span(items.IS.columns.values[1])
                if date == items.filing_date:
                    if period==1:
                        if check_DF_date_consistency(items.IS)==1:
                            return get_Combo_Account_by_Name(items.IS,dic)[1]
                        elif check_DF_date_consistency(items.IS)==2:
                            return get_Combo_Account_by_Name(items.IS,dic)[2]
                            print('Please check DFs on ',items.filing_date)
                        else:
                            print('Could not handle inconsistency')
                            return 0
                    elif period>1:
                        return get_Combo_Account_by_Name(series.DF[i].IS,dic)[1]-get_Combo_Account_by_Name(series.DF[i+1].IS,dic)[2]
        else:
            for i,items in enumerate(series.DF):
                period = span(items.IS.columns.values[1])
                if date == items.filing_date:
                    if period==1:
                        if check_DF_date_consistency(items.IS)==1:
                            return get_Combo_Account_by_Index(items.IS,index)[1]
                        elif check_DF_date_consistency(items.IS)==2:
                            return get_Combo_Account_by_Name(items.IS,index)[2]
                            print('Please check DFs on ',items.filing_date)
                        else:
                            print('Could not handle inconsistency')
                            return 0
                    elif period>1:
                        return get_Combo_Account_by_Index(series.DF[i].IS,index)[1]-get_Combo_Account_by_Index(series.DF[i+1].IS,index)[2]
    except KeyError:
        return 0
    except TypeError:
        return 0
    return None

### Get Quarterly Result for Specified Accounts and Date in the CF ###
def get_Q_Account_CF(series,date,index=[],dic=[]):
    try:
        if index == []:
            for i,items in enumerate(series.DF):
                period = span(items.CF.columns.values[1])
                if date == items.filing_date:
                    if period==1:
                        if check_DF_date_consistency(items.CF)==1:
                            return get_Combo_Account_by_Name(items.CF,dic)[1]
                        elif check_DF_date_consistency(items.CF)==2:
                            return get_Combo_Account_by_Name(items.CF,dic)[2]
                            print('Please check DFs on ',items.filing_date)
                        else:
                            print('Could not handle inconsistency')
                            return 0
                    elif period>1:
                        return get_Combo_Account_by_Name(series.DF[i].CF,dic)[1]-get_Combo_Account_by_Name(series.DF[i+1].CF,dic)[1]
        else:
            for i,items in enumerate(series.DF):
                period = span(items.CF.columns.values[1])
                if date == items.filing_date:
                    if period==1:
                        if check_DF_date_consistency(items.CF)==1:
                            return get_Combo_Account_by_Index(items.CF,index)[1]
                        elif check_DF_date_consistency(items.CF)==2:
                            return get_Combo_Account_by_Name(items.CF,index)[2]
                            print('Please check DFs on ',items.filing_date)
                        else:
                            print('Could not handle inconsistency')
                            return 0
                    elif period>1:
                        return get_Combo_Account_by_Index(series.DF[i].CF,index)[1]-get_Combo_Account_by_Index(series.DF[i+1].CF,index)[1]
    except KeyError:
        return 0
    except TypeError:
        return 0
    return None

### Get End on Quarter Result for Specified Accounts and Date in the BS ###
def get_Q_Account_BS(series,date,index=[],dic=[]):
    try:
        if index == []:
            for i,items in enumerate(series.DF):
                if date == items.filing_date:
                    return get_Combo_Account_by_Name(series.DF[i].BS,dic)[1]
        else:
            for i,items in enumerate(series.DF):
                if date == items.filing_date:
                    return get_Combo_Account_by_Index(series.DF[i].BS,index)[1]
    except KeyError:
        return 0
    except TypeError:
        return 0
    return None

### Sum Multiple Lines in a Statement by Name - e.g. Depreciation and Amortization ###
def get_Combo_Account_by_Name(DF,dic,name=''):
    #exmample DF: CF
    #exmaple name = 'DA'
    #example dic = ['Deprec','Amort','Exaust','amort','deprec','exaust']
    width = len(DF.columns.values)
    values = [0]*width
    values[0] = name
    i=0
    for i in range(0,len(DF.index.values)):
        if max([any(x in DF.iloc[i,0] for x in dic)]):
            for j in range(1,width):
                try:
                    values[j] += DF.iloc[i,j]
                except ValueError:
                    values[j] += 0
                except TypeError:
                    values[j] += 0
                except KeyError:
                    values[j] += 0
    return values

### Sum Multiple Lines in a Statement by Index - e.g. EBIT ###
def get_Combo_Account_by_Index(DF,index,name=''):
    #exmample DF: IS
    #exmaple name = 'EBIT'
    #example index = ['3.05']
    width = len(DF.columns.values)
    values = [0]*width
    values[0] = name
    i=0
    for i in index:
        for j in range(1,width):
            try:
                values[j] += DF.loc[i][j]
            except ValueError:
                values[j] += 0
            except TypeError:
                values[j] += 0
            except KeyError:
                values[j] += 0
    return values

### Data Frame With Unadjusted EBITDAs ###
def get_Q_EBITDA(series):
    l = list()
    for i in series.DF:
        try:
            EBITDA =  get_Q_Account_CF(series,i.filing_date,dic=['Deprec'])\
                        +get_Q_Account_IS(series,i.filing_date,index=['3.05'])
            l.append(i.filing_date)
            l.append(EBITDA)
        except TypeError:
            EBITDA = None
        except IndexError:
            EBITDA = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','EBITDA']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['EBITDA']=pd.to_numeric(df['EBITDA']).round(2)
    return df

### Data Frame With Unadjusted LTM EBITDAs ###
def get_Q_EBITDA_LTM_Release_Date(series):
    l = list()
    Q_EBITDA = get_Q_EBITDA(series)
    for i in range(4,len(Q_EBITDA)+1):
        try:
            EBITDA_LTM = float(Q_EBITDA[(i-4):i].sum())
            l.append(series.DF[len(Q_EBITDA)-i].delivery_date[0:10])
            l.append(EBITDA_LTM)
        except TypeError:
            EBITDA_LTM = None
        except IndexError:
            EBITDA_LTM = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','EBITDA_LTM'])
    df.set_index('Date',inplace=True)
    df['EBITDA_LTM']=pd.to_numeric(df['EBITDA_LTM']).round(2)
    return df

### Data Frame With Unadjusted EBITDAs as of Release Date ###
def get_Q_EBITDA_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            EBITDA =  get_Q_Account_CF(series,i.filing_date,dic=['Deprec'])\
                        +get_Q_Account_IS(series,i.filing_date,index=['3.05'])
            l.append(i.delivery_date[0:10])
            l.append(EBITDA)
        except TypeError:
            EBITDA = None
        except IndexError:
            EBITDA = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','EBITDA']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['EBITDA']=pd.to_numeric(df['EBITDA']).round(2)
    return df

### Data Frame With Unadjusted Revenue ###
def get_Q_Revenue(series):
    l = list()
    for i in series.DF:
        try:
            Revenue =  get_Q_Account_IS(series,i.filing_date,index=['3.01'])
            l.append(i.filing_date)
            l.append(Revenue)
        except TypeError:
            Revenue = None
        except IndexError:
            Revenue = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Revenue']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['Revenue']=pd.to_numeric(df['Revenue']).round(2)
    return df

### Data Frame With Unadjusted Revenue as of Release Date ###
def get_Q_Revenue_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            Revenue =  get_Q_Account_IS(series,i.filing_date,index=['3.01'])
            l.append(i.delivery_date[0:10])
            l.append(Revenue)
        except TypeError:
            Revenue = None
        except IndexError:
            Revenue = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Revenue']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['Revenue']=pd.to_numeric(df['Revenue']).round(2)
    return df

### Data Frame With Unadjusted LTM Revenue ###
def get_Q_Revenue_LTM_Release_Date(series):
    l = list()
    Q_Revenue = get_Q_Revenue(series)
    for i in range(4,len(Q_Revenue)+1):
        try:
            Revenue_LTM = float(Q_Revenue[(i-4):i].sum())
            l.append(series.DF[len(Q_Revenue)-i].delivery_date[0:10])
            l.append(Revenue_LTM)
        except TypeError:
            Revenue_LTM = None
        except IndexError:
            Revenue_LTM = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Revenue_LTM'])
    df.set_index('Date',inplace=True)
    df['Revenue_LTM']=pd.to_numeric(df['Revenue_LTM']).round(2)
    return df

### Data Frame With Unadjusted COGS ###
def get_Q_COGS(series):
    l = list()
    for i in series.DF:
        try:
            COGS =  get_Q_Account_IS(series,i.filing_date,index=['3.02'])
            l.append(i.filing_date)
            l.append(COGS)
        except TypeError:
            COGS = None
        except IndexError:
            COGS = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','COGS']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['COGS']=pd.to_numeric(df['COGS']).round(2)
    return df

### Data Frame With Unadjusted COGS as of Release Date ###
def get_Q_COGS_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            COGS =  get_Q_Account_IS(series,i.filing_date,index=['3.02'])
            l.append(i.delivery_date[0:10])
            l.append(COGS)
        except TypeError:
            COGS = None
        except IndexError:
            COGS = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','COGS']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['COGS']=pd.to_numeric(df['COGS']).round(2)
    return df

### Data Frame With Unadjusted LTM COGS ###
def get_Q_COGS_LTM_Release_Date(series):
    l = list()
    Q_COGS = get_Q_COGS(series)
    for i in range(4,len(Q_COGS)+1):
        try:
            COGS_LTM = float(Q_COGS[(i-4):i].sum())
            l.append(series.DF[len(Q_COGS)-i].delivery_date[0:10])
            l.append(COGS_LTM)
        except TypeError:
            COGS_LTM = None
        except IndexError:
            COGS_LTM = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','COGS_LTM'])
    df.set_index('Date',inplace=True)
    df['COGS_LTM']=pd.to_numeric(df['COGS_LTM']).round(2)
    return df

### Data Frame With Unadjusted SGA_Other ###
def get_Q_SGA_Other(series):
    l = list()
    for i in series.DF:
        try:
            SGA_Other =  get_Q_Account_IS(series,i.filing_date,index=['3.04'])
            l.append(i.filing_date)
            l.append(SGA_Other)
        except TypeError:
            SGA_Other = None
        except IndexError:
            SGA_Other = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','SGA_Other']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['SGA_Other']=pd.to_numeric(df['SGA_Other']).round(2)
    return df

### Data Frame With Unadjusted SGA_Other as of Release Date ###
def get_Q_SGA_Other_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            SGA_Other =  get_Q_Account_IS(series,i.filing_date,index=['3.04'])
            l.append(i.delivery_date[0:10])
            l.append(SGA_Other)
        except TypeError:
            SGA_Other = None
        except IndexError:
            SGA_Other = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','SGA_Other']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['SGA_Other']=pd.to_numeric(df['SGA_Other']).round(2)
    return df

### Data Frame With Unadjusted LTM SGA_Other ###
def get_Q_SGA_Other_LTM_Release_Date(series):
    l = list()
    Q_SGA_Other = get_Q_SGA_Other(series)
    for i in range(4,len(Q_SGA_Other)+1):
        try:
            SGA_Other_LTM = float(Q_SGA_Other[(i-4):i].sum())
            l.append(series.DF[len(Q_SGA_Other)-i].delivery_date[0:10])
            l.append(SGA_Other_LTM)
        except TypeError:
            SGA_Other_LTM = None
        except IndexError:
            SGA_Other_LTM = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','SGA_Other_LTM'])
    df.set_index('Date',inplace=True)
    df['SGA_Other_LTM']=pd.to_numeric(df['SGA_Other_LTM']).round(2)
    return df

### Data Frame With Unadjusted EBIT ###
def get_Q_EBIT(series):
    l = list()
    for i in series.DF:
        try:
            EBIT =  get_Q_Account_IS(series,i.filing_date,index=['3.05'])
            l.append(i.filing_date)
            l.append(EBIT)
        except TypeError:
            EBIT = None
        except IndexError:
            EBIT = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','EBIT']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['EBIT']=pd.to_numeric(df['EBIT']).round(2)
    return df

### Data Frame With Unadjusted EBIT as of Release Date ###
def get_Q_EBIT_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            EBIT =  get_Q_Account_IS(series,i.filing_date,index=['3.05'])
            l.append(i.delivery_date[0:10])
            l.append(EBIT)
        except TypeError:
            EBIT = None
        except IndexError:
            EBIT = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','EBIT']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['EBIT']=pd.to_numeric(df['EBIT']).round(2)
    return df

### Data Frame With Unadjusted LTM EBIT ###
def get_Q_EBIT_LTM_Release_Date(series):
    l = list()
    Q_EBIT = get_Q_EBIT(series)
    for i in range(4,len(Q_EBIT)+1):
        try:
            EBIT_LTM = float(Q_EBIT[(i-4):i].sum())
            l.append(series.DF[len(Q_EBIT)-i].delivery_date[0:10])
            l.append(EBIT_LTM)
        except TypeError:
            EBIT_LTM = None
        except IndexError:
            EBIT_LTM = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','EBIT_LTM'])
    df.set_index('Date',inplace=True)
    df['EBIT_LTM']=pd.to_numeric(df['EBIT_LTM']).round(2)
    return df

### Data Frame With Unadjusted Earnings ###
def get_Q_Earnings(series):
    l = list()
    for i in series.DF:
        try:
            Earnings =  get_Q_Account_IS(series,i.filing_date,index=['3.09'])
            l.append(i.filing_date)
            l.append(Earnings)
        except TypeError:
            Earnings = None
        except IndexError:
            Earnings = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Earnings']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['Earnings']=pd.to_numeric(df['Earnings']).round(2)
    return df

### Data Frame With Unadjusted Earnings as of Release Date ###
def get_Q_Earnings_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            Earnings =  get_Q_Account_IS(series,i.filing_date,index=['3.09'])
            l.append(i.delivery_date[0:10])
            l.append(Earnings)
        except TypeError:
            Earnings = None
        except IndexError:
            Earnings = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Earnings']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['Earnings']=pd.to_numeric(df['Earnings']).round(2)
    return df

### Data Frame With Unadjusted LTM Earnings ###
def get_Q_Earnings_LTM_Release_Date(series):
    l = list()
    Q_Earnings = get_Q_Earnings(series)
    for i in range(4,len(Q_Earnings)+1):
        try:
            Earnings_LTM = float(Q_Earnings[(i-4):i].sum())
            l.append(series.DF[len(Q_Earnings)-i].delivery_date[0:10])
            l.append(Earnings_LTM)
        except TypeError:
            Earnings_LTM = None
        except IndexError:
            Earnings_LTM = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Earnings_LTM'])
    df.set_index('Date',inplace=True)
    df['Earnings_LTM']=pd.to_numeric(df['Earnings_LTM']).round(2)
    return df

### Data Frame With Unadjusted Financial Expense ###
def get_Q_Financial(series):
    l = list()
    for i in series.DF:
        try:
            Financial =  get_Q_Account_IS(series,i.filing_date,index=['3.06'])
            l.append(i.filing_date)
            l.append(Financial)
        except TypeError:
            Financial = None
        except IndexError:
            Financial = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Financial']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['Financial']=pd.to_numeric(df['Financial']).round(2)
    return df

### Data Frame With Unadjusted Financial Expense as of Release Date ###
def get_Q_Financial_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            Financial =  get_Q_Account_IS(series,i.filing_date,index=['3.06'])
            l.append(i.delivery_date[0:10])
            l.append(Financial)
        except TypeError:
            Financial = None
        except IndexError:
            Financial = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Financial']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['Financial']=pd.to_numeric(df['Financial']).round(2)
    return df

### Data Frame With Unadjusted LTM Financial Expense ###
def get_Q_Financial_LTM_Release_Date(series):
    l = list()
    Q_Financial = get_Q_Financial(series)
    for i in range(4,len(Q_Financial)+1):
        try:
            Financial_LTM = float(Q_Financial[(i-4):i].sum())
            l.append(series.DF[len(Q_Financial)-i].delivery_date[0:10])
            l.append(Financial_LTM)
        except TypeError:
            Financial_LTM = None
        except IndexError:
            Financial_LTM = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Financial_LTM'])
    df.set_index('Date',inplace=True)
    df['Financial_LTM']=pd.to_numeric(df['Financial_LTM']).round(2)
    return df

### Data Frame With Quarterly FCFF ###
def get_Q_FCFF(series):
    l = list()
    for i in series.DF:
        try:
            #CFO + I*(1-t) - Capex
            I = -get_Q_Account_IS(series,i.filing_date,index=['3.06'])
            T = -get_Q_Account_IS(series,i.filing_date,index=['3.08'])
            EBT = get_Q_Account_IS(series,i.filing_date,index=['3.07'])
            try:
                t = max(T/EBT,0.34)
            except ZeroDivisionError:
                t = 0
            FCFF =  get_Q_Account_CF(series,i.filing_date,index=['6.01'])\
                    +I*(1-t)\
                    +get_Q_Account_CF(series,i.filing_date,index=['6.02'])
            l.append(i.filing_date)
            l.append(FCFF)
        except TypeError:
            FCFF = None
        except IndexError:
            FCFF = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','FCFF']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['FCFF']=pd.to_numeric(df['FCFF']).round(2)
    return df

### Data Frame With Unadjusted FCFF as of Release Date ###
def get_Q_FCFF_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            #CFO + I*(1-t) - Capex
            I = -get_Q_Account_IS(series,i.filing_date,index=['3.06'])
            T = -get_Q_Account_IS(series,i.filing_date,index=['3.08'])
            EBT = get_Q_Account_IS(series,i.filing_date,index=['3.07'])
            try:
                t = max(T/EBT,0.34)
            except ZeroDivisionError:
                t = 0
            FCFF =  get_Q_Account_CF(series,i.filing_date,index=['6.01'])\
                    +I*(1-t)\
                    +get_Q_Account_CF(series,i.filing_date,index=['6.02'])
            l.append(i.delivery_date[0:10])
            l.append(FCFF)
        except TypeError:
            FCFF = None
        except IndexError:
            FCFF = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','FCFF']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['FCFF']=pd.to_numeric(df['FCFF']).round(2)
    return df

### Data Frame With Unadjusted LTM FCFF ###
def get_Q_FCFF_LTM_Release_Date(series):
    l = list()
    Q_FCFF = get_Q_FCFF_Release_Date(series)
    for i in range(4,len(Q_FCFF)+1):
        try:
            FCFF_LTM = float(Q_FCFF[(i-4):i].sum())
            l.append(series.DF[len(Q_FCFF)-i].delivery_date[0:10])
            l.append(FCFF_LTM)
        except TypeError:
            FCFF_LTM = None
        except IndexError:
            FCFF_LTM = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','FCFF_LTM'])
    df.set_index('Date',inplace=True)
    df['FCFF_LTM']=pd.to_numeric(df['FCFF_LTM']).round(2)
    return df

### Data Frame With Net Debt ###
def get_Q_Net_Debt(series):
    l = list()
    for i in series.DF:
        try:
            Net_Debt =  get_Q_Account_BS(series,i.filing_date,index=['2.01.04','2.02.01'])\
                        - get_Q_Account_BS(series,i.filing_date,index=['1.01.01','1.01.02'])
            l.append(i.filing_date)
            l.append(Net_Debt)
        except TypeError:
            Net_Debt = None
        except IndexError:
            Net_Debt = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Net_Debt'])
    df.set_index('Date',inplace=True)
    df = df.iloc[::-1]
    df['Net_Debt']=pd.to_numeric(df['Net_Debt']).round(2)
    return df

### Data Frame With Net Debt as of Release Date ###
def get_Q_Net_Debt_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            Net_Debt =  get_Q_Account_BS(series,i.filing_date,index=['2.01.04','2.02.01'])\
                        - get_Q_Account_BS(series,i.filing_date,index=['1.01.01','1.01.02'])
            l.append(i.delivery_date[0:10])
            l.append(Net_Debt)
        except TypeError:
            Net_Debt = None
        except IndexError:
            Net_Debt = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Net_Debt'])
    df.set_index('Date',inplace=True)
    df = df.iloc[::-1]
    df['Net_Debt']=pd.to_numeric(df['Net_Debt']).round(2)
    return df

### Net Debt to EBITDA as of Release Date ###
def get_Q_Net_Debt_EBITDA(series):
    t = get_Q_Net_Debt_Release_Date(series).join(get_Q_EBITDA_LTM_Release_Date(series),on='Date')
    df = (((t.loc[:,'Net_Debt']/t.loc[:,'EBITDA_LTM']).dropna()).round(2)).to_frame()
    df.columns = ['Net_Debt_to_EBITDA']
    return df

def get_Q_EV_Bridge(series):
    l = list()
    for i in series.DF:
        try:
            EV_Bridge =  get_Q_Account_BS(series,i.filing_date,index=['2.01.04','2.02.01'])\
                        - get_Q_Account_BS(series,i.filing_date,index=['1.01.01','1.01.02'])
            l.append(i.delivery_date[0:10])
            l.append(EV_Bridge)
        except TypeError:
            EV_Bridge = None
        except IndexError:
            EV_Bridge = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','EV_Bridge'])
    df.set_index('Date',inplace=True)
    df = df.iloc[::-1]
    df['EV_Bridge']=pd.to_numeric(df['EV_Bridge']).round(2)
    return df

### Data Frame With Past EV Bridges as of Release Dates ###
def get_Q_EV_Bridge_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            EV_Bridge =  get_Q_Account_BS(series,i.filing_date,index=['2.01.04','2.02.01','2.03.09'])\
                        -get_Q_Account_BS(series,i.filing_date,index=['1.01.01','1.01.02'])
            l.append(i.delivery_date[0:10])
            l.append(EV_Bridge)
        except TypeError:
            EV_Bridge = None
        except IndexError:
            EV_Bridge = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','EV_Bridge']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['EV_Bridge']=pd.to_numeric(df['EV_Bridge']).round(2)
    return df

### Book Value as of Release Date ###
def get_Q_BV_Release_Date(series):
    l = list()
    for i in series.DF:
        try:
            BV = get_Q_Account_BS(series,i.filing_date,index=['2.03'])\
            -get_Q_Account_BS(series,i.filing_date,index=['2.03.09'])
            l.append(i.delivery_date[0:10])
            l.append(BV)
        except TypeError:
            BV = None
        except IndexError:
            BV = None
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Book_Value']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['Book_Value']=pd.to_numeric(df['Book_Value']).round(2)
    return df

### Market Cap ###
def get_Q_Market_Cap_Release_Date(series):
    # In thousands of reais
    l = list()
    for i in series.DF:
        try:
            date = convert_date(i.delivery_date[0:10])
            if series.ticker_preferred==None:
                Market_Cap = i.common_shares*series.common_prices['Close'][date]/1000
            else:
                Market_Cap = (i.common_shares*series.common_prices['Close'][date]\
                +i.preferred_shares*series.preferred_prices['Close'][date])/1000
            l.append(i.delivery_date[0:10])
            l.append(Market_Cap)
        except TypeError:
            Market_Cap = None
        except IndexError:
            Market_Cap = None
        except KeyError:
            date = convert_date(i.delivery_date[0:10])
            date = nearest(series.common_prices.index,datetime.strptime(date,'%Y-%m-%d'))
            if series.ticker_preferred==None:
                Market_Cap = i.common_shares*series.common_prices['Close'][date]/1000
            else:
                Market_Cap = (i.common_shares*series.common_prices['Close'][date]\
                +i.preferred_shares*series.preferred_prices['Close'][date])/1000
            l.append(i.delivery_date[0:10])
            l.append(Market_Cap)
    df = pd.DataFrame(np.array(l).reshape(int(len(l)/2),2), columns = ['Date','Market_Cap']).iloc[::-1]
    df.set_index('Date',inplace=True)
    df['Market_Cap']=pd.to_numeric(df['Market_Cap']).round(2)
    return df

### EV to EBITDA ###
def get_Q_EV_EBITDA_Release_Date(series):
    t = get_Q_Market_Cap_Release_Date(series).join(get_Q_EV_Bridge_Release_Date(series))
    t = t.join(get_Q_EBITDA_LTM_Release_Date(series))
    df = ((((t.loc[:,'Market_Cap']+t.loc[:,'EV_Bridge'])/t.loc[:,'EBITDA_LTM']\
            ).dropna()).round(2)).to_frame()
    df.columns = ['EV_to_EBITDA']
    return df

### Price to Book ###
def get_Q_Price_BV_Release_Date(series):
    t = get_Q_Market_Cap_Release_Date(series).join(get_Q_BV_Release_Date(series))
    df = (((t.loc[:,'Market_Cap']/t.loc[:,'Book_Value']\
            ).dropna()).round(2)).to_frame()
    df.columns = ['Price_to_BV']
    return df

### Plot Series ###
def series_Plot(method, series, multiple = False):
    y = method(series).iloc[:,0].values.tolist()
    x = method(series).index.values.tolist()
    y_mean = [np.mean(y)]*len(x)
    short_date=list()
    for i in x:
        if i[3]=='0':
            short_date.append(i[4:6]+i[8:10])
        else:
            short_date.append(i[3:6]+i[8:10])
    x = list(range(0,len(short_date)))
    fig,ax = plt.subplots()
    if multiple == False:
        y = [x / 1000.0 for x in y]
        y_mean = [x / 1000.0 for x in y_mean ]
        plt.title(method.__name__[4:]+' R$MM')
    else:
        plt.title(method.__name__[4:]+' x')
    plt.xticks(x[::2], short_date[::2])
    ax.plot(x,y, label='Data', marker='o')
    ax.plot(x,y_mean, label='Mean', linestyle='--')
    ax.legend(loc='upper right')
    plt.show()
    fig.savefig("graph.png")

### Summary of Valuation Estimates ###
def series_Val(series, wacc=0.12, inflation = 0.045, real_growth = 0.02):
    cs = series.DF[0].common_shares/1000 #In thousands
    cp = series.common_prices['Close'].iloc[-1]
    if series.ticker_preferred!=None:
        ps = series.DF[0].preferred_shares/1000 #In thousands
        pp = series.preferred_prices['Close'].iloc[-1]
        rp = 1/(cp/pp*cs+ps)
        rc = 1/(pp/cp*ps+cs)
    else:
        pp = 0
        rp = 0
        rc = 1/cs
    y = get_Q_FCFF_LTM_Release_Date(series)
    r = get_Q_Revenue_LTM_Release_Date(series).iloc[:,0].values.tolist()
    e = get_Q_EV_EBITDA_Release_Date(series).iloc[-1,0]
    EV_Bridge = get_Q_EV_Bridge(series).iloc[-1,0]
    result = seasonal_decompose(y, model='additive',freq=1)
    f_trend = result.trend.dropna() # Trend without NaNs
    try:
        g = min(real_growth+inflation,pow(float(r[len(r)-1])/float(r[0]),4.0/len(r))-1)
    except TypeError:
        g = real_growth+inflation
    except ZeroDivisionError:
        g = real_growth+inflation       
    g_steady_state = inflation
    trend_max_growth = (float(f_trend.max())*(1+g)/(wacc-g)-EV_Bridge)
    trend_max_no_growth = (float(f_trend.max())*(1+g_steady_state)/(wacc-g_steady_state)-EV_Bridge)
    trend_avg_growth = (float(f_trend.mean())*(1+g)/(wacc-g)-EV_Bridge)
    trend_avg_no_growth = (float(f_trend.mean())*(1+g_steady_state)/(wacc-g_steady_state)-EV_Bridge)
    ltm_growth = ((y.iloc[-1,0])\
                        *(1+g)/(wacc - g)\
                        -EV_Bridge)
    ltm_no_growth = ((y.iloc[-1,0])\
                        *(1+g_steady_state)/(wacc - g_steady_state)\
                        -EV_Bridge)
    bvps = get_Q_BV_Release_Date(series).iloc[-1,0]
    avg = sum([trend_max_growth,trend_max_no_growth,trend_avg_growth,trend_avg_no_growth,ltm_growth,ltm_no_growth,bvps])/7
    date = series.common_prices.index[-1].date().__str__()
    print('WACC BRL:       %.1f' % (100*wacc),'%')
    print('Nominal growth: %.1f' % (100*g),'%')
    print('Zero growth:    %.1f' % (100*g_steady_state),'%')
    print('********************************Valuation*****************************************')
    print('Maximum trend FCFF w/ actual nominal growth          Common: %.2f' % (trend_max_growth*rc), end=' | ')
    print('Pref: %.2f' % (trend_max_growth*rp))
    print('Maximum trend FCFF w/ growth matching inflation      Common: %.2f' % (trend_max_no_growth*rc), end = ' | ')
    print('Pref: %.2f' % (trend_max_no_growth*rp))
    print('Avgerage trend FCFF w/ actual nominal growth         Common: %.2f' % (trend_avg_growth*rc), end=' | ')
    print('Pref: %.2f' % (trend_avg_growth*rp))
    print('Avgerage trend FCFF w/ growth matching inflation     Common: %.2f' % (trend_avg_no_growth*rc), end=' | ')
    print('Pref: %.2f' % (trend_avg_no_growth*rp))
    print('LTM FCFF w/ actual nominal growth                    Common: %.2f' % (ltm_growth*rc), end=' | ')
    print('Pref: %.2f' % (ltm_growth*rp))
    print('LTM FCFF w/ growth matching inflation                Common: %.2f' % (ltm_no_growth*rc), end=' | ')
    print('Pref: %.2f' % (ltm_no_growth*rp))
    print('Book value per share                                 Common: %.2f' % (bvps*rc), end=' | ')
    print('Pref: %.2f' % (bvps*rp))
    print('**********************************************************************************')
    print('Average                                              Common: %.2f' % (avg*rc), end=' | ')
    print('Pref: %.2f' % (avg*rp))
    print('Current price as of %s                       Common: %.2f' % (date,cp), end=' | ')
    print('Pref: %.2f' % pp)
    print('Current EV / EBITDA LTM:                                         %.2f x' % e)

### Load Pre-Saved Object ###
def series_Open(file_name):
    with open(file_name +'.pkl', 'rb') as input:
            return pickle.load(input)

class DFs:
    def __init__(self,cod, filing_date, delivery_date):
        self.cod = cod
        self.filing_date = filing_date
        self.delivery_date = delivery_date
        self.preferred_shares = None
        self.common_shares = None
        self.IS = None
        self.CF = None
        self.BS = None

class DFs_Series:
    def __init__(self,ticker_common,ccvm,cnpj,ticker_preferred=None):
        self.DF = list()
        self.ccvm = ccvm
        self.cnpj = cnpj
        self.name = ticker_common
        self.ticker_common = ticker_common
        self.ticker_preferred = ticker_preferred
        self.common_prices = None
        self.preferred_prices = None
    ### Get DF Series ###
    def get_DFs(self, last_n):
        ccvm = self.ccvm
        cnpj = self.cnpj
        self.DF = get_series(self.DF,ccvm,cnpj,last_n)
    def get_Prices(self):
        yf.pdr_override() #Temporary
        self.common_prices = pdr.get_data_yahoo(self.ticker_common+'.SA',start='2010-01-01')
        if self.ticker_preferred != None:
            self.preferred_prices = pdr.get_data_yahoo(self.ticker_preferred+'.SA',start='2010-01-01')
        else:
            self.preferred_prices = 0
    def series_save(self):
        with open(self.name+'.pkl', 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

