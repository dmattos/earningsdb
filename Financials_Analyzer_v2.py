# -*- coding: utf-8 -*-
"""
Created on Sun Sep 19 10:35:06 2021

@author: lins_
"""

import pandas as pd
import requests
import io
import zipfile
import numpy as np

r = requests.get('http://dados.cvm.gov.br/dados/CIA_ABERTA/CAD/DADOS/cad_cia_aberta.csv')
diretorio = 'D:/CVM_Data'

tipos_de_arquivos = ['_cia_aberta_',    #0
'_cia_aberta_BPA_con_',                 #1
'_cia_aberta_BPA_ind_',                 #2
'_cia_aberta_BPP_con_',                 #3
'_cia_aberta_BPP_ind_',                 #4
'_cia_aberta_DFC_MD_con_',              #5
'_cia_aberta_DFC_MD_ind_',              #6
'_cia_aberta_DFC_MI_con_',              #7
'_cia_aberta_DFC_MI_ind_',              #8
'_cia_aberta_DMPL_con_',                #9
'_cia_aberta_DMPL_ind_',                #10
'_cia_aberta_DRA_con_',                 #11
'_cia_aberta_DRA_ind_',                 #12
'_cia_aberta_DRE_con_',                 #13
'_cia_aberta_DRE_ind_',                 #14
'_cia_aberta_DVA_con_',                 #15
'_cia_aberta_DVA_ind_']                 #16

ano = '2023'


link_DFP = 'http://dados.cvm.gov.br/dados/CIA_ABERTA/DOC/DFP/DADOS/'+'dfp'+tipos_de_arquivos[0]+ano+'.zip'
link_ITR = 'http://dados.cvm.gov.br/dados/CIA_ABERTA/DOC/ITR/DADOS/'+'itr'+tipos_de_arquivos[0]+ano+'.zip'


r.text

#r = requests.get(link_DFP)

#zf = zipfile.ZipFile(io.BytesIO(r.content))
zf = zipfile.ZipFile('D:/CVM_Data/'+'itr'+tipos_de_arquivos[0]+ano+'.zip')
arquivo = 'itr' + tipos_de_arquivos[0] + ano +'.csv'
zf = zf.open(arquivo)
lines = zf.readlines()
lines = [ i.strip().decode('ISO-8859-1') for i in lines ]
lines = [ i.split(';') for i in lines]
df = pd.DataFrame(lines[1:],columns=lines[0])

def read_file(fname,zipfile):
    lines = zipfile.open(fname).readlines()
    lines = [ i.strip().decode('ISO-8859-1') for i in lines ]
    lines = [ i.split(';') for i in lines]
    return pd.DataFrame(lines[1:],columns=lines[0])
    
def format_date(date_str):
    return (date_str.iloc[0][8:10]+'/'+date_str.iloc[0][5:7]+'/'+date_str.iloc[0][0:4])

def convert_ITR_CVM_format(ref_df,ccvm,year,is_BP=False,is_CF=False):
    ret_list = [] 
    ref_df = ref_df.loc[ref_df.loc[:,'CD_CVM']==ccvm,:]
    #.loc[ref_df.loc[:,'VERSAO']==ref_df.loc[df.loc[:,'CD_CVM']==ccvm,:].VERSAO.unique().max(),:]
    
    #1st quarter
    if(is_BP==False): 
        col_1 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-03-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-03-31'),'VL_CONTA'].values
        col_2 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-03-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-03-31'),'VL_CONTA'].values
        col_3 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-03-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-03-31'),'DS_CONTA'].values
        col_4 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-03-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-03-31'),'CD_CONTA'].values
        d = {'Conta':col_4,'Descrição':col_3,('01/01/'+str(year) +' a 31/03/'+str(year)):col_2,('01/01/'+str(year-1) +' a 31/03/'+str(year-1)):col_1}
    else: 
        col_1 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-03-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-12-31'),'VL_CONTA'].values
        col_2 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-03-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-03-31'),'VL_CONTA'].values
        col_3 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-03-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-03-31'),'DS_CONTA'].values
        col_4 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-03-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-03-31'),'CD_CONTA'].values
        d = {'Conta':col_4,'Descrição':col_3,('31/03/'+str(year)):col_2,('31/12/'+str(year-1)):col_1}
    aux = pd.DataFrame(d).set_index('Conta')
    if(len(aux)!=0):
        ret_list.append(aux)
    
    #2nd quarter
    if(is_BP==False): 
        col_1 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-06-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year-1)+'-01-01'),'VL_CONTA'].values
        if(is_CF==False): col_2 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-06-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year-1)+'-04-01'),'VL_CONTA'].values
        col_3 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year)+'-01-01'),'VL_CONTA'].values
        if(is_CF==False): col_4 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year)+'-04-01'),'VL_CONTA'].values
        col_5 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year)+'-01-01'),'DS_CONTA'].values
        col_6 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year)+'-01-01'),'CD_CONTA'].values      
        if(is_CF==False):d = {'Conta':col_6,'Descrição':col_5,('01/04/'+str(year) +' a 30/06/'+str(year)):col_4,('01/01/'+str(year) +' a 30/06/'+str(year)):col_3,('01/04/'+str(year-1) +' a 30/06/'+str(year-1)):col_2,('01/01/'+str(year-1) +' a 30/06/'+str(year-1)):col_1}
        else: d = {'Conta':col_6,'Descrição':col_5,('01/01/'+str(year) +' a 30/06/'+str(year)):col_3,('01/01/'+str(year-1) +' a 30/06/'+str(year-1)):col_1}
    else: 
        col_1 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-12-31'),'VL_CONTA'].values
        col_2 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-06-30'),'VL_CONTA'].values
        col_3 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-06-30'),'DS_CONTA'].values
        col_4 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-06-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-06-30'),'CD_CONTA'].values
        d = {'Conta':col_4,'Descrição':col_3,('30/06/'+str(year)):col_2,('31/12/'+str(year-1)):col_1}
    try:
        aux = pd.DataFrame(d).set_index('Conta')
    except ValueError:
        aux = pd.DataFrame([])
    if(len(aux)!=0):
        ret_list.append(aux)
    
    #3rd quarter
    if(is_BP==False): 
        col_1 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-09-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year-1)+'-01-01'),'VL_CONTA'].values
        if(is_CF==False): col_2 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-09-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year-1)+'-07-01'),'VL_CONTA'].values
        col_3 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year)+'-01-01'),'VL_CONTA'].values
        if(is_CF==False): col_4 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year)+'-07-01'),'VL_CONTA'].values
        col_5 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year)+'-01-01'),'DS_CONTA'].values
        col_6 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_INI_EXERC']==(str(year)+'-01-01'),'CD_CONTA'].values
        if(is_CF==False): d = {'Conta':col_6,'Descrição':col_5,('01/07/'+str(year) +' a 30/09/'+str(year)):col_4,('01/01/'+str(year) +' a 30/09/'+str(year)):col_3,('01/07/'+str(year-1) +' a 30/09/'+str(year-1)):col_2,('01/01/'+str(year-1) +' a 30/09/'+str(year-1)):col_1}
        else: d = {'Conta':col_6,'Descrição':col_5,('01/01/'+str(year) +' a 30/09/'+str(year)):col_3,('01/01/'+str(year-1) +' a 30/09/'+str(year-1)):col_1}
    else: 
        col_1 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-12-31'),'VL_CONTA'].values
        col_2 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-09-30'),'VL_CONTA'].values
        col_3 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-09-30'),'DS_CONTA'].values
        col_4 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-09-30'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-09-30'),'CD_CONTA'].values
        d = {'Conta':col_4,'Descrição':col_3,('30/09/'+str(year)):col_2,('31/12/'+str(year-1)):col_1}
    try:
        aux = pd.DataFrame(d).set_index('Conta')
    except ValueError:
        aux = pd.DataFrame([])    
    if(len(aux)!=0):
        ret_list.append(aux)    
    return ret_list
    
def convert_DFP_CVM_format(ref_df,ccvm,year,is_BP=False,is_CF=False):
    ret_list = []
    ref_df = ref_df.loc[ref_df.loc[:,'CD_CVM']==ccvm,:]
    
    #1st quarter
    col_1 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-12-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year-1)+'-12-31'),'VL_CONTA'].values
    col_2 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-12-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-12-31'),'VL_CONTA'].values
    col_3 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-12-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-12-31'),'DS_CONTA'].values
    col_4 = ref_df.loc[ref_df.loc[:,'DT_REFER']==(str(year)+'-12-31'),:].loc[ref_df.loc[:,'DT_FIM_EXERC']==(str(year)+'-12-31'),'CD_CONTA'].values
    if(is_BP==False): d = {'Conta':col_4,'Descrição':col_3,('01/01/'+str(year) +' a 31/12/'+str(year)):col_2,('01/01/'+str(year-1) +' a 31/12/'+str(year-1)):col_1}
    else: d = {'Conta':col_4,'Descrição':col_3,('31/12/'+str(year)):col_2,('31/12/'+str(year-1)):col_1}
    try:
        aux = pd.DataFrame(d).set_index('Conta')
    except ValueError:
        aux = pd.DataFrame([])   
    if(len(aux)!=0):
        ret_list.append(aux)    
    return ret_list

CCR = DFs_Series('CCRO3',ccvm='018821',cnpj='02.846.056/0001-97')
CCR.get_DFs()

SLC = DFs_Series('SLCE3',ccvm='020745',cnpj='89.096.457/0001-55')
SLC.get_DFs()

